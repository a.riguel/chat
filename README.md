# Now I am here

#Doing Chanche in master

# New changes in feature11111


# Development
## Building:
`npm i`

`npm run watch`

## Running
Run the server with node `node server/index.js`
This serves both the websocket server and the static page.


# Deploying a Room
You can deploy your own chat room as long as it inherits from the contract `Room.sol`

If you want to have some metadata attached can set the `metadata` variable to an [IPFS](https://ipfs.io/) hash of a JSON object. 

Currently the only properties that are used are `"name"` and `"description"`.

You can also use the `tokenRoom` contract to deploy a room that only lets in addresses based on token balances. You just have to supply the address of the token in question (any ERC20 token) and the minimum amount of tokens an address should hold in order to participate.
